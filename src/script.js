let waveforms = []
let wavesurfer = []
let contenedor = document.querySelector('.audios')
let comenzar = document.querySelector('.miBtn')

for (let i = 0; i < 10; i++) {
    let x = Math.floor(Math.random() * (contenedor.offsetWidth * 0.8))
    let y = 0
    waveforms.push(document.createElement('div'))
    waveforms[i].id = `waveform${i}`
    waveforms[i].style.position = 'relative'
    waveforms[i].className = 'onda'

    waveforms[i].style.marginLeft = x + 'px'
    waveforms[i].style.marginTop = (-y) + 'px'

    contenedor.appendChild(waveforms[i])

    wavesurfer[i] = WaveSurfer.create({
        container: `#waveform${i}`,
        waveColor: 'violet',
        progressColor: 'purple',
        hideScrollbar: true,
        cursorWidth: 0
    });

    wavesurfer[i].load('../assets/voz.mp3');

    waveforms[i].addEventListener('mouseenter', function (e) {
        wavesurfer[i].play();
    })

    waveforms[i].addEventListener('mouseleave', function (e) {
        wavesurfer[i].stop();
    })

    waveforms[i].addEventListener('click', function (e) {
        window.location.href = 'src/tts/tts.html'
    })
}

comenzar.addEventListener('click', function(e){
    contenedor.style.opacity = 1
    comenzar.style.display = 'none'
})