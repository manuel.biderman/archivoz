# ArchiVoz
_Archivoz_ es un proyecto de [Intercambios Transorgánicos](https://intercambiostransorganicos.org/), equipo de diseño y desarrollo de interfaces.
El interés principal de este proyecto es reinterpretar y colaborar con comprender problemáticas sociales, y en particular, del campo de la salud vocal.
La App _“Archivoz”_ es un archivo de voces dinámico que funciona como un _sistema de visualización de datos y como una aplicación “text to speech”._

Basado en una implementacion de https://github.com/mathigatti/tts-web 

![Arquitectura Back + Front](imgs/_IT__Archivoz__arquitectura_de_solución.jpg)



## Repositorio para el front-end del proyecto



### Instrucciones
1. Clone repo
2. Run live server

## Docker

```  
docker build -t archivoz .
docker run archivoz
``` 

## Contribuciones
Pull requests son bienvenidas. Para cambios mayores, por favor abrir un issue para evaluarlos.

